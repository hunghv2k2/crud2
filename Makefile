up:
	docker-compose up

down:
	docker-compose down

tidy:
	cd src/public && go mod tidy
	cd src/migration && go mod tidy

test:
	cd src/public && go mod tidy && go test ./...

swagger-internal:
	cd src/internal && swag init --parseDependency --parseDepth=3

swagger-public:
	cd src/public && swag init --parseDependency --parseDepth=3
