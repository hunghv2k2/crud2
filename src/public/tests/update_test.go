package tests

import (
	golibdataTestUtil "gitlab.com/golibs-starter/golib-data/testutil"
	golibtest "gitlab.com/golibs-starter/golib-test"
	"net/http"
	"testing"
)

func TestUpdate_ShouldSuccess(t *testing.T) {
	golibdataTestUtil.TruncateTables("restaurants")

	createbody := `{
         "name": "PositionToken",
 		 "address": "Vietnam"
   		}`

	golibtest.NewRestAssured(t).
		When().
		Post("/v1/restaurants").
		Body(createbody).
		Then().
		Status(http.StatusCreated).
		Body("meta.code", 201).
		Body("data.name", "PositionToken").
		Body("data.address", "Vietnam")

	golibdataTestUtil.AssertDatabaseHas(t, "restaurants", map[string]interface{}{
		"name":    "PositionToken",
		"address": "Vietnam",
	})

	updatebody := `{
         "name": "Position Token",
 		 "address": "Viet Nam"
   		}`

	golibtest.NewRestAssured(t).
		When().
		Patch("/v1/restaurants/1").
		Body(updatebody).
		Then().
		Status(http.StatusOK).
		Body("meta.code", 200).
		Body("meta.message", "Successful").
		Body("data.name", "Position Token").
		Body("data.address", "Viet Nam")

	golibdataTestUtil.AssertDatabaseHas(t, "restaurants", map[string]interface{}{
		"name":    "Position Token",
		"address": "Viet Nam",
	})
}
