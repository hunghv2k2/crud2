package tests

import (
	"fmt"
	"gitlab.com/backend/service/public/models"
	golibdataTestUtil "gitlab.com/golibs-starter/golib-data/testutil"
	golibtest "gitlab.com/golibs-starter/golib-test"
	"net/http"
	"testing"
)

func TestGetOrderById_WhenOrderIdIsInvalid_ShouldError(t *testing.T) {
	golibdataTestUtil.TruncateTablesOpt("restaurants")
	golibtest.NewRestAssured(t).
		When().
		Get("/v1/restaurants/xxx").
		Then().
		Status(http.StatusBadRequest).
		Body("meta.code", 400).
		Body("meta.message", "Bad Request")
}

func TestGetOrderById_WhenOrderIdIsValid_ShouldReturnSuccess(t *testing.T) {
	golibdataTestUtil.TruncateTablesOpt("restaurants")
	expectedOrder := models.RestaurantModel{
		ID:      1,
		Name:    "PositionToken",
		Address: "Vietnam",
	}
	golibtest.NewRestAssured(t).
		When().
		Get(fmt.Sprintf("/v1/restaurants/%d", expectedOrder.ID)).
		Then().
		Status(http.StatusOK).
		Body("meta.code", 200).
		Body("data.id", expectedOrder.ID).
		Body("data.name", expectedOrder.Name).
		Body("data.address", expectedOrder.Address)
}

func TestGetOrderById_WhenOrderIdIsInvalid_ShouldReturnNotFound(t *testing.T) {
	expectedOrder := models.RestaurantModel{
		ID:      11,
		Name:    "local",
		Address: "somewhere",
	}

	golibtest.NewRestAssured(t).
		When().
		Get(fmt.Sprintf("/v1/restaurants/%d", expectedOrder.ID)).
		Then().
		Status(http.StatusNotFound).
		Body("meta.code", 404).
		Body("meta.message", "Resource Not Found")
}
