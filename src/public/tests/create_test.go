package tests

import (
	golibdataTestUtil "gitlab.com/golibs-starter/golib-data/testutil"
	golibtest "gitlab.com/golibs-starter/golib-test"
	"net/http"
	"testing"
)

func TestCreateRestaurants_ResponseSuccess_ShouldReturnSuccess(t *testing.T) {
	golibdataTestUtil.TruncateTables("restaurants")

	createbody := `{
         "name": "PositionToken",
 		 "address": "Vietnam"
   		}`

	golibtest.NewRestAssured(t).
		When().
		Post("/v1/restaurants").
		Body(createbody).
		Then().
		Status(http.StatusCreated).
		Body("meta.code", 201).
		Body("data.name", "PositionToken").
		Body("data.address", "Vietnam")

	golibdataTestUtil.AssertDatabaseHas(t, "restaurants", map[string]interface{}{
		"name":    "PositionToken",
		"address": "Vietnam",
	})
}
