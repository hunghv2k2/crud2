package tests

import (
	"gitlab.com/backend/service/public/models"
	golibdataTestUtil "gitlab.com/golibs-starter/golib-data/testutil"
	golibtest "gitlab.com/golibs-starter/golib-test"
	"net/http"
	"testing"
)

func TestGetConnected_ShouldReturnSuccess(t *testing.T) {
	restaurants := []*models.RestaurantModel{
		{
			ID:      5,
			Name:    "localhost",
			Address: "vietnam",
		},
		{
			ID:      7,
			Name:    "ad",
			Address: "2ac",
		},
	}
	golibdataTestUtil.Insert(restaurants)
}

func TestGetAll_ShouldSuccess(t *testing.T) {
	golibdataTestUtil.TruncateTablesOpt("restaurants")
	golibtest.NewRestAssured(t).
		When().
		Get("/v1/restaurants").
		Then().
		Status(http.StatusOK).
		Body("meta.code", 200).
		Body("meta.message", "Successful").
		Body("data.0.id", 1).
		Body("data.0.name", "PositionToken").
		Body("data.0.address", "Vietnam").
		Body("data.1.id", 5).
		Body("data.1.name", "localhost").
		Body("data.1.address", "vietnam").
		Body("data.2.id", 7).
		Body("data.2.name", "ad").
		Body("data.2.address", "2ac").
		Body("data.#", 3)
}
