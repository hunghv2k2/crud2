package tests

import (
	"context"
	"gitlab.com/backend/service/public/bootstrap"
	"gitlab.com/golibs-starter/golib"
	golibdataTestUtil "gitlab.com/golibs-starter/golib-data/testutil"
	golibmigrate "gitlab.com/golibs-starter/golib-migrate"
	golibtest "gitlab.com/golibs-starter/golib-test"
	"go.uber.org/fx"
)

func init() {
	err := fx.New(
		bootstrap.All(),
		golibdataTestUtil.EnableDatabaseTestUtil(),
		golibmigrate.MigrationOpt(),
		golib.ProvidePropsOption(golib.WithActiveProfiles([]string{"testing"})),
		golib.ProvidePropsOption(golib.WithPaths([]string{"../config/"})),
		golibtest.EnableWebTestUtil(),
	).Start(context.Background())
	if err != nil {
		panic(err)
	}
}
