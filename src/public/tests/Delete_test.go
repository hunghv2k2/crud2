package tests

import (
	"fmt"
	"gitlab.com/backend/service/public/models"
	golibdataTestUtil "gitlab.com/golibs-starter/golib-data/testutil"
	golibtest "gitlab.com/golibs-starter/golib-test"
	"net/http"
	"testing"
)

func TestDelete_ShouldSuccess(t *testing.T) {
	golibdataTestUtil.TruncateTablesOpt("restaurants")
	expectedOrder := models.RestaurantModel{
		ID: 4,
	}
	golibtest.NewRestAssured(t).
		When().
		Delete(fmt.Sprintf("/v1/restaurants/%d", expectedOrder.ID)).
		Then().
		Status(http.StatusOK).
		Body("meta.code", 200).
		Body("data", true)
}
