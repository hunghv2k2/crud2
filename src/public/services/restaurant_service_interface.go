package services

import (
	"context"
	"gitlab.com/backend/service/public/models"
)

type RestaurantServiceInterface interface {
	GetAll(context context.Context) ([]*models.RestaurantModel, error)
	GetByID(context context.Context, id int) (*models.RestaurantModel, error)
	Create(context context.Context, model *models.RestaurantModel) (*models.RestaurantModel, error)
	DeleteByID(ctx context.Context, id int) error
	UpdateRestaurant(ctx context.Context, data *models.RestaurantModel, id int) (*models.RestaurantModel, error)
}
