package services

import (
	"context"
	"gitlab.com/backend/service/public/models"
	"gitlab.com/backend/service/public/repositories"
)

type RestaurantService struct {
	repo repositories.RestaurantRepositoryInterface
}

func NewRestaurantService(repo repositories.RestaurantRepositoryInterface) RestaurantServiceInterface {
	return &RestaurantService{repo: repo}
}

func (r *RestaurantService) GetAll(context context.Context) ([]*models.RestaurantModel, error) {
	restaurants, err := r.repo.GetAll(context)

	if err != nil {
		return nil, err
	}
	return restaurants, nil
}

func (r *RestaurantService) GetByID(context context.Context, id int) (*models.RestaurantModel, error) {
	restaurant, err := r.repo.GetByID(context, id)
	if err != nil {
		return nil, err
	}
	return restaurant, nil
}

func (r *RestaurantService) Create(context context.Context, model *models.RestaurantModel) (*models.RestaurantModel, error) {
	newdata, err := r.repo.Create(context, model)
	if err != nil {
		return nil, err
	}
	return newdata, err
}

func (r *RestaurantService) DeleteByID(context context.Context, id int) error {
	err := r.repo.DeleteByID(context, id)
	if err != nil {
		return err
	}
	return nil
}

func (r *RestaurantService) UpdateRestaurant(
	context context.Context,
	data *models.RestaurantModel,
	id int,
) (*models.RestaurantModel, error) {
	updatedata, err := r.repo.Update(context, data, id)
	if err != nil {
		return nil, err
	}

	return updatedata, nil
}
