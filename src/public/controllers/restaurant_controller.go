package controllers

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/backend/service/public/requests"
	"gitlab.com/backend/service/public/resources"
	"gitlab.com/backend/service/public/services"
	"gitlab.com/golibs-starter/golib/exception"
	"gitlab.com/golibs-starter/golib/log"
	"gitlab.com/golibs-starter/golib/web/response"
	"strconv"
)

type RestaurantController struct {
	restaurantService services.RestaurantServiceInterface
}

// NewRestaurantController return a new RestaurantController instance
func NewRestaurantController(restaurantService services.RestaurantServiceInterface) *RestaurantController {
	return &RestaurantController{
		restaurantService: restaurantService,
	}
}

func (c *RestaurantController) Get(ctx *gin.Context) {

	restaurants, err := c.restaurantService.GetAll(context.Background())
	if err != nil {
		log.Errorf("find restaurants error: %v", err)
		response.WriteError(ctx.Writer, exception.SystemError)
		return
	}

	resp := make([]*resources.RestaurantResource, 0, len(restaurants))
	for _, restaurant := range restaurants {
		resp = append(resp, new(resources.RestaurantResource).FromEntity(restaurant))
	}

	response.Write(ctx.Writer, response.Ok(resp))
}

func (c *RestaurantController) GetByID(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("ID"))
	if err != nil {
		response.WriteError(ctx.Writer, exception.BadRequest)
		return
	}

	restaurant, err := c.restaurantService.GetByID(context.Background(), id)
	if restaurant == nil {
		response.WriteError(ctx.Writer, exception.NotFound)
		return
	}
	if err != nil {
		log.Errorf("find restaurants error: %v", err)
		response.WriteError(ctx.Writer, exception.SystemError)
		return
	}

	response.Write(ctx.Writer, response.Ok(new(resources.RestaurantResource).FromEntity(restaurant)))
}

func (c *RestaurantController) Create(ctx *gin.Context) {

	var body requests.RestaurantRequest
	if err := ctx.ShouldBindJSON(&body); err != nil {
		response.WriteError(ctx.Writer, exception.BadRequest)
		return
	}

	restaurant, err := c.restaurantService.Create(context.Background(), body.ToEntity())
	if err != nil {
		log.Errorf("create restaurants %+v error: %v", body, err)
		response.WriteError(ctx.Writer, exception.SystemError)
		return
	}

	response.Write(ctx.Writer, response.Created(new(resources.RestaurantResource).FromEntity(restaurant)))
}

func (c *RestaurantController) Update(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("ID"))
	if err != nil {
		response.WriteError(ctx.Writer, exception.BadRequest)
		return
	}

	var body requests.RestaurantRequest
	if err := ctx.ShouldBindJSON(&body); err != nil {
		response.WriteError(ctx.Writer, exception.BadRequest)
		return
	}

	restaurant, err := c.restaurantService.UpdateRestaurant(context.Background(), body.ToEntity(), id)

	if restaurant == nil {
		response.WriteError(ctx.Writer, exception.NotFound)
		return
	}

	if err != nil {
		log.Errorf("Update restaurants %+v error: %v", body, err)
		response.WriteError(ctx.Writer, exception.SystemError)
		return
	}

	response.Write(ctx.Writer, response.Ok(new(resources.RestaurantResource).FromEntity(restaurant)))
}

func (c *RestaurantController) Delete(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("ID"))
	if err != nil {
		response.WriteError(ctx.Writer, exception.BadRequest)
		return
	}

	if err := c.restaurantService.DeleteByID(ctx.Request.Context(), id); err != nil {
		log.Errorf("find restaurants error: %v", err)
		response.WriteError(ctx.Writer, exception.SystemError)
		return
	}

	response.Write(ctx.Writer, response.Ok(true))
}
