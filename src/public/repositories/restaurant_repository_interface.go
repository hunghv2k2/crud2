package repositories

import (
	"context"
	"gitlab.com/backend/service/public/models"
)

type RestaurantRepositoryInterface interface {
	GetAll(ctx context.Context) ([]*models.RestaurantModel, error)
	GetByID(ctx context.Context, id int) (*models.RestaurantModel, error)
	Create(ctx context.Context, data *models.RestaurantModel) (*models.RestaurantModel, error)
	DeleteByID(ctx context.Context, id int) error
	Update(ctx context.Context, data *models.RestaurantModel, id int) (*models.RestaurantModel, error)
}
