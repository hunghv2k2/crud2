package main

import (
	"gitlab.com/backend/service/public/bootstrap"
	"go.uber.org/fx"
)

// @title API
// @version 1.0.0
func main() {
	fx.New(bootstrap.All()).Run()
}
