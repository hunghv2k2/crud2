package bootstrap

import (
	"gitlab.com/backend/service/public/controllers"
	"gitlab.com/backend/service/public/properties"
	"gitlab.com/backend/service/public/repositories"
	"gitlab.com/backend/service/public/routers"
	"gitlab.com/backend/service/public/services"
	"gitlab.com/golibs-starter/golib"
	golibdata "gitlab.com/golibs-starter/golib-data"
	"gitlab.com/golibs-starter/golib-gin"
	"go.uber.org/fx"
)

// All register all constructors for fx container
func All() fx.Option {
	return fx.Options(
		golib.AppOpt(),
		golib.PropertiesOpt(),
		golib.LoggingOpt(),
		golib.EventOpt(),
		golib.BuildInfoOpt(Version, CommitHash, BuildTime),
		golib.ActuatorEndpointOpt(),
		golib.ProvideProps(properties.NewSwaggerProperties),
		golibgin.GinHttpServerOpt(),

		golibdata.DatasourceOpt(),

		fx.Provide(repositories.NewRestaurantRepository),

		fx.Provide(services.NewRestaurantService),
		fx.Provide(controllers.NewRestaurantController),

		fx.Invoke(routers.RegisterHandlers),
		fx.Invoke(routers.RegisterGinRouters),

		// Graceful shutdown.
		// OnStop hooks will run in reverse order.
		// golib.OnStopEventOpt() MUST be first
		golib.OnStopEventOpt(),
		golibgin.OnStopHttpServerOpt(),
	)
}
