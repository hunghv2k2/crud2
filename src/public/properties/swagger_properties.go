package properties

import (
	"gitlab.com/golibs-starter/golib/config"
)

// NewSwaggerProperties create a new SwaggerProperties instance
func NewSwaggerProperties(loader config.Loader) (*SwaggerProperties, error) {
	props := SwaggerProperties{}
	err := loader.Bind(&props)
	return &props, err
}

// SwaggerProperties represent swagger configuration properties
type SwaggerProperties struct {
	Enabled bool
}

// Prefix return yaml prefix of configuration
func (t *SwaggerProperties) Prefix() string {
	return "app.swagger"
}
