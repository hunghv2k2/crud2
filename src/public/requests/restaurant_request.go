// Source code template generated by https://gitlab.com/technixo/tnx

package requests

import (
	"gitlab.com/backend/service/public/models"
)

// RestaurantRequest represents
type RestaurantRequest struct {
	Name    string `json:"name,omitempty"`
	Address string `json:"address,omitempty"`
}

func (r *RestaurantRequest) ToEntity() *models.RestaurantModel {
	return &models.RestaurantModel{
		Name:    r.Name,
		Address: r.Address,
	}
}
